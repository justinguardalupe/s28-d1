console.log("Hello World");

// Asynchronous Statement
// TheFetch API allows to asynchronously request from a resource(data)
console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

fetch('https://jsonplaceholder.typicode.com/posts').then(response => console.log(response.status));

console.log("Goodbye");

// Retrieve contents/data from the "Response" object
fetch('https://jsonplaceholder.typicode.com/posts').then((response)=> response.json()).then((json) => console.log(json));

